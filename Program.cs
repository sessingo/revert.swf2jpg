﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SWFToImage;

namespace SWF2JPG
{
	class Program
	{
		public static string InputFile { get; set; }
		public static string OutputFile { get; set; }
		static void Main(string[] args)
		{

			/* Parse arguments */
			for(var i=0;i<args.Length;i++)
			{
				if(args[i]=="-i" && args[i+1] != null)
				{
					InputFile = args[i + 1];
				} else if (args[i] == "-o" && args[i + 1] != null)
				{
					OutputFile = args[i + 1];
				}
			}

			if (string.IsNullOrEmpty(InputFile))
			{
				Console.WriteLine("Missing required argument input-file (-i)");
				return;
			}

			if (string.IsNullOrEmpty(OutputFile))
			{
				Console.WriteLine("Missing required argument output-file (-o)");
				return;
			}

			var obj = new SWFToImageObject
			{
			    JPEGQuality = 100,
			    ImageWidth = 640,
			    ImageHeight = 480,
			    InputSWFFileName = @InputFile
			};

			// input SWF file
			obj.Execute_Begin();
			obj.FrameIndex = obj.FramesCount;
			obj.ImageOutputType = TImageOutputType.iotJPG;
			obj.Execute_End();
			obj.Execute();
			obj.SaveToFile(@OutputFile); // save to bmp file
			Console.Write("Completed!");
			Environment.Exit(0);
		}
	}
}
